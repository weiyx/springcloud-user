package cn.weiyx.user.domain.user;

import org.springframework.data.jpa.repository.JpaRepository;
import cn.weiyx.user.domain.user.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
