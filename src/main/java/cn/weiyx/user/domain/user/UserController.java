package cn.weiyx.user.domain.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	
	private final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping("/{id}")
	public User findById(@PathVariable("id") Long id){
		return userRepository.findOne(id);
	}
	
	@RequestMapping("/user.id.get")
	public User findById(@RequestParam Long id, @RequestParam String name){
		LOGGER.error("name = " + name);
		return userRepository.findOne(id);
	}

}
